vdr-plugin-dummydevice (2.0.0-6) experimental; urgency=medium

  * Build-depend on vdr 2.6.0 and update standards version to 4.6.0
  * Remove Peter Siering, Thomas Günther and Thomas Schmidt from uploaders

 -- Tobias Grimm <etobi@debian.org>  Thu, 13 Jan 2022 19:22:20 +0100

vdr-plugin-dummydevice (2.0.0-5) experimental; urgency=medium

  * Add Lintian override for error about not beeing linked to libc
  * Update debhelper-compat to 13
  * Build-depend on vdr >= 2.4.7

 -- Tobias Grimm <etobi@debian.org>  Mon, 20 Dec 2021 10:25:11 +0100

vdr-plugin-dummydevice (2.0.0-4) experimental; urgency=medium

  * Build-depend on vdr-dev (>= 2.4.0)

 -- Tobias Grimm <etobi@debian.org>  Mon, 16 Apr 2018 19:34:54 +0200

vdr-plugin-dummydevice (2.0.0-3) experimental; urgency=medium

  * VCS moved to salsa.debian.org
  * Standards-Version: 4.1.3
  * Replaced priority extra with optional
  * Dropped vdr-plugin-dummydevice-dbg - migration to automatically created
    debug symbol package

 -- Tobias Grimm <etobi@debian.org>  Sun, 18 Feb 2018 17:05:26 +0100

vdr-plugin-dummydevice (2.0.0-2) experimental; urgency=medium

  * Now supporting the /etc/vdr/conf.d mechanism
  * Standards-Version: 3.9.6
  * Build-depend on vdr-dev (>= 2.2.0)

 -- Tobias Grimm <etobi@debian.org>  Wed, 25 Mar 2015 21:50:19 +0100

vdr-plugin-dummydevice (2.0.0-1) experimental; urgency=medium

  * New upstream release

 -- Tobias Grimm <etobi@debian.org>  Mon, 09 Feb 2015 19:11:33 +0100

vdr-plugin-dummydevice (1.0.3-5) experimental; urgency=low

  * Added vdr-plugin-dummydevice-dbg package

 -- Tobias Grimm <etobi@debian.org>  Mon, 01 Apr 2013 20:25:16 +0200

vdr-plugin-dummydevice (1.0.3-4) experimental; urgency=low

  * Build-depend on vdr-dev (>= 2.0.0)

 -- Tobias Grimm <etobi@debian.org>  Sun, 31 Mar 2013 13:58:49 +0200

vdr-plugin-dummydevice (1.0.3-3) experimental; urgency=low

  * Build-depend on vdr-dev (>= 1.7.42)

 -- Tobias Grimm <etobi@debian.org>  Sat, 23 Mar 2013 20:51:56 +0100

vdr-plugin-dummydevice (1.0.3-2) experimental; urgency=low

  * Standards-Version: 3.9.4
  * Build-depend in vdr-dev (>= 1.7.40)
  * Use debhelper 9

 -- Tobias Grimm <etobi@debian.org>  Tue, 19 Mar 2013 22:41:16 +0100

vdr-plugin-dummydevice (1.0.3-1) experimental; urgency=low

  * New upstream release
  * Dropped fPIC patch
  * Standards-Version: 3.9.2

 -- Tobias Grimm <etobi@debian.org>  Mon, 12 Dec 2011 21:13:39 +0100

vdr-plugin-dummydevice (1.0.2-17) experimental; urgency=low

  * Switched to GIT using pristine tar

 -- Tobias Grimm <etobi@debian.org>  Sat, 29 Oct 2011 19:51:01 +0200

vdr-plugin-dummydevice (1.0.2-16) experimental; urgency=low

  * Removed non-standard shebang line from debian/rules
  * Added README.source
  * Standards-Version: 3.8.3

 -- Tobias Grimm <etobi@debian.org>  Mon, 09 Nov 2009 19:36:23 +0100

vdr-plugin-dummydevice (1.0.2-15) experimental; urgency=low

  * Release for vdrdevel 1.7.6
  * Added Tobias Grimm and myself to Uploaders
  * Updated debian/copyright to our new format
  * Added ${misc:Depends}
  * Bumped standards version to 3.8.1
  * Changed section to "video"

 -- Thomas Günther <tom@toms-cafe.de>  Wed, 29 Apr 2009 23:10:49 +0200

vdr-plugin-dummydevice (1.0.2-14) experimental; urgency=low

  * Dropped patchlevel control field
  * Build-Depend on vdr-dev (>=1.6.0-5)
  * Bumped Standards-Version to 3.8.0

 -- Tobias Grimm <tg@e-tobi.net>  Fri, 25 Jul 2008 19:24:17 +0200

vdr-plugin-dummydevice (1.0.2-13) experimental; urgency=low

  * Increased package version to force rebuild for vdr 1.6.0-1ctvdr7

 -- Tobias Grimm <tg@e-tobi.net>  Mon, 12 May 2008 12:52:53 +0200

vdr-plugin-dummydevice (1.0.2-12) experimental; urgency=low

  * Build-depend on vdr-dev (>= 1.6.0)
  * Switched Build-System to cdbs, Build-Depend on cdbs
  * Added Homepage field to debian/control
  * Renamed XS-Vcs-* fields to Vcs-* in debian/control
  * Bumped Standards-Version to 3.7.3

 -- Tobias Grimm <tg@e-tobi.net>  Thu, 03 Apr 2008 23:32:46 +0200

vdr-plugin-dummydevice (1.0.2-11) experimental; urgency=low

  * Force rebuild for vdr 1.5.15

 -- Tobias Grimm <tg@e-tobi.net>  Mon, 18 Feb 2008 21:07:37 +0100

vdr-plugin-dummydevice (1.0.2-10) unstable; urgency=low

  * Release for vdrdevel 1.5.13

 -- Thomas Günther <tom@toms-cafe.de>  Wed, 16 Jan 2008 10:31:29 +0100

vdr-plugin-dummydevice (1.0.2-9) unstable; urgency=low

  * Release for vdrdevel 1.5.12

 -- Thomas Günther <tom@toms-cafe.de>  Tue, 20 Nov 2007 23:46:14 +0100

vdr-plugin-dummydevice (1.0.2-8) unstable; urgency=low

  * Release for vdrdevel 1.5.11

 -- Thomas Günther <tom@toms-cafe.de>  Tue,  6 Nov 2007 23:34:08 +0100

vdr-plugin-dummydevice (1.0.2-7) unstable; urgency=low

  * Release for vdrdevel 1.5.10

 -- Thomas Günther <tom@toms-cafe.de>  Tue, 16 Oct 2007 23:50:50 +0200

vdr-plugin-dummydevice (1.0.2-6) unstable; urgency=low

  * Release for vdrdevel 1.5.9

 -- Thomas Günther <tom@toms-cafe.de>  Tue, 28 Aug 2007 01:01:01 +0200

vdr-plugin-dummydevice (1.0.2-5) unstable; urgency=low

  * Release for vdrdevel 1.5.8

 -- Thomas Günther <tom@toms-cafe.de>  Thu, 23 Aug 2007 01:08:55 +0200

vdr-plugin-dummydevice (1.0.2-4) unstable; urgency=low

  * Release for vdrdevel 1.5.6

 -- Thomas Günther <tom@toms-cafe.de>  Tue, 14 Aug 2007 01:46:09 +0200

vdr-plugin-dummydevice (1.0.2-3) unstable; urgency=low

  * New version to force update with new VDR package release

 -- Tobias Grimm <tg@e-tobi.net>  Sun,  8 Jul 2007 21:34:17 +0200

vdr-plugin-dummydevice (1.0.2-1) unstable; urgency=low

  * Initial release

 -- Peter Siering <ps@ctmagazin.de>  Thu, 21 Jun 2007 00:11:52 +0200

